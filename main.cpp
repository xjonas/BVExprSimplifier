
#include <iostream>
#include <string>
#include <z3++.h>
#include <cmath>
#include <fstream>
#include <getopt.h>

#include <chrono>

#include "UnconstrainedVariableSimplifier.h"
#include "ExprSimplifier.h"

using namespace std;
using namespace z3;

using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;

int main(int argc, char* argv[])
{
    static struct option long_options[] = {
	{"propagate-unconstrained", no_argument, 0, 'p' },
	{"count-locally", no_argument, 0, 'l' },
	{"canonize", no_argument, 0, 'c' },
	{"run-z3", no_argument, 0, 'z' },
	{"inprocess", no_argument, 0, 'i' },
	{"dag-counting", no_argument, 0, 'd' },
	{"full", no_argument, 0, 'f' },
	{"mulReplacementMode", required_argument, 0, 'r' },
	{"mulReplacement", required_argument, 0, 'm' },
	{0,           0,                 0,  0   }
    };

    bool propagateUnconstrainedFlag = false, runZ3 = false, canonize = false, countVariablesLocally = false, inprocess = false, full = false, dagCounting = false;
    char* filename;
    MulReplacementMode mulReplacementMode = SHIFT;
    MulReplacement mulReplacement = ALL;

    int opt = 0;

    int long_index = 0;
    while ((opt = getopt_long(argc, argv,"plzcifdm:", long_options, &long_index )) != -1)
    {
	switch (opt)
	{
	case 'p':
	    propagateUnconstrainedFlag = true;
	    break;
	case 'l':
	    countVariablesLocally = true;
	    break;
	case 'z':
	    runZ3 = true;
	    break;
	case 'i':
	    inprocess = true;
	    break;
	case 'c':
	    canonize = true;
	    break;
	case 'f':
	    full = true;
	    break;
	case 'd':
	    dagCounting = true;
	    break;
	case 'r':
	{
	    string argString(optarg);
	    if (argString == "mul")
	    {
		mulReplacementMode = MUL;
		//std::cout << "Mode: mul" << std::endl;
	    }
	    else if (argString == "shift")
	    {
		mulReplacementMode = SHIFT;
		//std::cout << "Mode: shift" << std::endl;
	    }
	    break;
	}
	case 'm':
	{
	    string argString(optarg);
	    if (argString == "odd")
	    {
		mulReplacement = ODD;
		//std::cout << "Mul: odd" << std::endl;
	    }
	    else if (argString == "linear")
	    {
		mulReplacement = LINEAR;
		//std::cout << "Mul: linear" << std::endl;
	    }
	    else if (argString == "all")
	    {
		mulReplacement = ALL;
		//std::cout << "Mul: all" << std::endl;
	    }
	    break;
	}
	default:
	    std::cout << "Invalid arguments" << std::endl;
	    exit(1);
	}
    }

    if (optind < argc)
    {
	filename = argv[optind];
    }
    else
    {
	std::cout << "Filename required" << std::endl;
	return 1;
    }

    z3::context ctx;
    Z3_ast ast = Z3_parse_smtlib2_file(ctx, filename, 0, 0, 0, 0, 0, 0);
    expr e = to_expr(ctx, ast);

    e = e.simplify();

    if (canonize)
    {
        ExprSimplifier simplifier(ctx);
        e = simplifier.PushNegations(e);

	UnconstrainedVariableSimplifier uSimplifier(ctx, e);
	e = uSimplifier.CanonizeBoundVariables(e);
    }

    if (full)
    {
        ExprSimplifier simplifier(ctx, propagateUnconstrainedFlag, countVariablesLocally);
	e = simplifier.Simplify(e);
    }
    else if (propagateUnconstrainedFlag)
    {
	unsigned oldHash = 0;

	//do
	//{
	//    oldHash = e.hash();
	    UnconstrainedVariableSimplifier simplifier(ctx, e);
	    simplifier.SetCountVariablesLocally(countVariablesLocally);
	    simplifier.SetMulReplacementMode(mulReplacementMode);
	    simplifier.SetMulReplacement(mulReplacement);
	    simplifier.SetDagCounting(dagCounting);
	    simplifier.SimplifyIte();
	    e = simplifier.GetExpr();

	    /*    if (inprocess)
	    {
		z3::goal g(ctx);
		g.add(e);

		z3::tactic derTactic =
		    z3::tactic(ctx, "simplify") &
		    z3::tactic(ctx, "elim-and") &
		    z3::tactic(ctx, "der") &
		    z3::tactic(ctx, "simplify") &
		    z3::tactic(ctx, "distribute-forall") &
		    z3::tactic(ctx, "simplify");

		z3::apply_result result = derTactic(g);

		assert(result.size() == 1);
		z3::goal simplified = result[0];
		e = simplified.as_expr();
	    }
	    } while (oldHash != e.hash()); */
    }

    e = e.simplify();
    //std::cout << e << std::endl;

    solver s = tactic(ctx, "bv").mk_solver();
    params p(ctx);

    //	p.set(":max_memory", 3u);
    s.set(p);

    s.add(e);
    if (runZ3)
    {
	std::cout << s.check() << std::endl;
    }
    else
    {
        std::cout << "(set-logic BV)" << std::endl;
	//std::cout << s.to_smt2() << std::endl;

	std::cout << s << std::endl;
	std::cout << "(check-sat)" << std::endl;
    }
}
